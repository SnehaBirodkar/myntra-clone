const Footer = () => {
    return (
        <>
            <div className="" style={{ background: "#ff3e6c", padding: "10px", color: "#fff", textAlign: "center"}}>
                <h4>Designed and developed by Sneha Birodkar</h4>
            </div>
        </>
    )
}

export default Footer;